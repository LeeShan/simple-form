#!/bin/bash
echo "### Updating apt"
apt-get update
echo "### Installing LAMP stack and xdebug for development"
apt-get -y install htop mc apache2 php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-gd php7.0-intl php-pear php-imagick php7.0-imap php7.0-mcrypt php-memcache  php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-mbstring php-gettext php7.0-opcache php-apcu php-xdebug
apt-get -y install htop mc apache2 php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-gd php7.0-intl php-pear php-imagick php7.0-imap php7.0-mcrypt php-memcache  php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-mbstring php-gettext php7.0-opcache php-apcu php-xdebug

echo "### installing mysql in non-interactive mode"
dpkg-reconfigure -f noninteractive
echo "mysql-server-5.6 mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server-5.6 mysql-server/root_password_again password root" | sudo debconf-set-selections
apt-get -y install mysql-server mysql-client

echo "### Creating default.local VHost environment"
echo "1.2.3.4 default.local" >> /etc/hosts
ln -s /vagrant/code /var/www/html/code
cp /vagrant/provision/vhost-template.conf /etc/apache2/sites-available/default.local.conf
a2ensite default.local
service apache2 restart