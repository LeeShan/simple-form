# Simple form with not so simple implementation

All the tutorials out their try to show you how to create a from and save it to a database.
This task can be easily achieve by writing a simple form and save the result. But most of the time you need a framework to do so.

Use YII/YII2/Laravel/A framework!!!!

This example shows you this is not an easy task if you want to do it right.

## Getting started

To start this project you need to clone this repo, install VirtualBox, Vagrant.

This is a multi box environtment. By default only ubuntu16 box will start. And it has a basic provision script to create a basic LAMP stack.

You can start to Ubutnu 16.04 box with

    vagrant up

or

    vagrant up ubunut16

## After the box started

Login to the box with 
    
    vagrant ssh

Or with your prefered ssh client. The default credentials are: vagrant:vagrant.

You need to change to root to continue by:

    sudo su

Run the bootstras.sh located at:

    /vagrant/code/default.local/bootstrap.sh

You may need to change this file to be an executable with:

    chmod +x /vagrant/code/default.local/bootstrap.sh

These steps can be run in the initial step in production, but I want you to try out in this development environment.

## Running the application

If you already added defaul.local to your host file, then you can visit : [http://default.local](http://default.local) and the up should be up and running.

If not you can add this entry with root/system administrator rights to your host file:

Append the file with:

    1.2.3.4 default.local

on Windows:

    c:\Windows\System32\drivers\etc\hosts

on Linux:

    /etc/hosts

Or any above failed you can still run it on: [http://1.2.3.4/code/default.local/src/](http://1.2.3.4/code/default.local/src/)

