<?php

namespace SimpleForm\app\models;

use SimpleForm\framework\MysqlConnection;

class User
{
    protected $userName;
    protected $errors;
    protected $post;
    protected $savedMessage;
    public function __construct($post)
    {
        if (empty($post)) {
            return;
        }
        $this->post = $post;
        if ($this->validate()) {
            $this->save();
        }
    }
    protected function validate()
    {
        if (!isset($this->post['submit'])) {
            return false;
        }
        if (!$this->validateUserName()) {
            return false;
        }
        return true;
    }
    protected function save()
    {
        $db = new MysqlConnection(dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.ini');
        $connection = $db->connect();
        $stmt = $connection->prepare("INSERT INTO User (name) VALUES (?)");
        $stmt->bind_param("s", $this->userName);
        if (!$stmt->execute()) {
            $this->errors['db_error'] = "Failed to save. Error number [{$connection->errno}] error string [{$connection->error}]";
            return false;
        }
        $this->savedMessage = "New record created successfully. Last inserted ID is: " . $connection->insert_id;
        $stmt->close();
        return true;
    }
    protected function validateUserName()
    {
        if (!isset($this->post['userName'])) {
            $this->errors['userName'] = 'User name must be set';
            return false;
        }
        $this->userName = $this->post['userName'];
        if (strlen($this->post['userName']) < 5) {
            $this->errors['userName'] = 'User name must be at least 5 character long.';
            return false;
        }
        return true;
    }
    public function success()
    {
        return !is_null($this->savedMessage) ? $this->savedMessage : '';
    }
    public function getUserName()
    {
        return $this->userName;
    }
    public function getUserNameError()
    {
        return isset($this->errors['userName']) ? $this->errors['userName'] : '';
    }
    public function getErrors()
    {
        return empty($this->errors) ? '' : var_export($this->errors, true);
    }
}
