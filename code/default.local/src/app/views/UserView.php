<?php
namespace SimpleForm\app\views;
class UserView
{
    protected $model;
    public function __construct($model)
    {
        $this->model = $model;
    }
    public function render()
    {
        $model = $this->model;
        include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'_userview.php';
    }
}