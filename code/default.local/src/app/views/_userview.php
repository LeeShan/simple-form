<html>  
      <head>  
           <title>Webslesson Tutorial | PHP Server Side Form Validation</title>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
      </head>  
      <body>  
           <br />  
           <div class="container" style="width:500px;">  
                <h3 class="text-center">PHP Server Side Form Validation</h3>  
                <form method="post">
                    <div class="form-group">  
                          <label>Felhasználónév<span class="text-danger">*</span></label>  
                          <input type="text" name="userName" class="form-control" minlength="3" maxlength="16" value="<?= htmlentities($model->getUserName()) ?>"/>  
                          <span class="text-danger"><?= htmlentities($model->getUserNameError()) ?></span>  
                     </div>   
                     
                     <div class="form-group">  
                          <input type="submit" name="submit" value="Submit" class="btn btn-info" />  
                     </div>  
                </form>  
                <div><?= htmlentities($model->getErrors()) ?></div>
                <div><?= htmlentities($model->success()) ?></div>
           </div>  
           <br />  
      </body>  
 </html>  