<?php
namespace SimpleForm\app\controllers;

use SimpleForm\app\views\UserView;
use SimpleForm\app\models\User;

class Controller
{
    public function actionIndex()
    {
        $view = new UserView(new User($_POST?:[]));
        $view->render();
    }   
}
