<?php

namespace SimpleForm\framework;

abstract class AbstractMigration
{
    /**
     * Undocumented variable
     *
     * @var MysqlConnection
     */
    protected $db;

    public function __construct()
    {
        $this->db = new MysqlConnection(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.ini');
    }
    abstract public function migrateUp();
    abstract public function migrateDown();
}
