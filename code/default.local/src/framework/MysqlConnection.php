<?php

namespace SimpleForm\framework;

use mysqli;

class MysqlConnection
{
    protected $configs;
    /**
     * Undocumented variable
     *
     * @var mysqli
     */
    protected $connection;

    /**
     * Undocumented variable
     *
     * @var ConsoleLogger
     */
    protected $log;
    
    public function __construct($configIniFile)
    {
        $this->configs = parse_ini_file($configIniFile);
        $this->log = new ConsoleLogger();
    }

    public function connectRaw()
    {
        $this->connection = mysqli_connect($this->configs['host'], $this->configs['user'], $this->configs['password']);
        return $this->connection;
    }

    public function connect()
    {
        $this->connection = mysqli_connect($this->configs['host'], $this->configs['user'], $this->configs['password'], $this->configs['dbname']);
        return $this->connection;
    }

    public function __destruct()
    {
        if (is_resource($this->connection)) {
            mysqli_close($this->connection);
        }
    }
    public function query($sql)
    {
        $this->log->debug("Running query [$sql]");
        if (is_null($this->connection)) {
            if (!$this->connect()) {
                $this->log->error("Error when creating connection [" . mysql_error($this->connection) . "]");
                return mysqli_errno($this->connection);
            }
        }
        if (!$this->connection->query($sql)) {
            $this->log->error("Error when running query: [$sql]. Error:  [" . mysqli_error($this->connection) . "]");
            return mysqli_errno($this->connection);
        }
        return true;
    }
}
