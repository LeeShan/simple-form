<?php
namespace SimpleForm\migrations;

use SimpleForm\framework\AbstractMigration;

class InitMigaration extends AbstractMigration
{
    public function migrateUp()
    {
        // We don't have database yet. We need a raw connetion to create it.
        $this->db->connectRaw();
        $this->db->query('CREATE DATABASE test CHARACTER SET utf8 COLLATE utf8_general_ci');
        // change connection to database only.
        // @todo Create separated user without root database credential and use that access.
        // Usually it's not done with php and it's not wise to use it.
        $this->db->connect();
        $this->db->query('CREATE TABLE User (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
            name VARCHAR(30) NOT NULL
        )'); 
    }

    public function migrateDown()
    {
        $this->db->connect();
        $this->db->query('DROP TABLE User');  
        $this->db->connectRaw();
        $this->db->query('DROP DATABASE test');
    }
}