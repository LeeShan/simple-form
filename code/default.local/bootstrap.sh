#!/bin/bash
echo "### installing composer"
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
composer -V

echo "### installing dependencies with composer"
composer install

echo "Bootstraping test application"
php src/migrations/MigrateUp.php